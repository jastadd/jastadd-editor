#!/bin/sh

SRC='update-site'
HOST='login.cs.lth.se'
TARGET='/cs/jastadd/update-site/eclipse-syntax'
COPY_CMD='scp -r '$SRC'/* '$HOST':'$TARGET
ACCESS_CMD='ssh '$HOST' chmod 664 '$TARGET'/site.xml'
ACCESS_CMD2='ssh '$HOST' chmod 664 '$TARGET'/artifacts.jar'
ACCESS_CMD3='ssh '$HOST' chmod 664 '$TARGET'/content.jar'
ACCESS_CMD4='ssh '$HOST' chmod 664 '$TARGET'/plugins/*.jar'
ACCESS_CMD5='ssh '$HOST' chmod 664 '$TARGET'/features/*.jar'

echo ' This will publish the update site for eclipse-syntax: '
echo '   from: ['$SRC']'
echo '   to:   ['$TARGET']'
echo '   on:   ['$HOST']'
echo '   using: ['$COPY_CMD']'
echo '          ['$ACCESS_CMD']'
echo '          ['$ACCESS_CMD2']'
echo '          ['$ACCESS_CMD3']'
echo '          ['$ACCESS_CMD4']'
echo '          ['$ACCESS_CMD5']'
echo ''
echo ' NB! Remember to update plugin/feature version and build update site before updating.'
echo ''
read -r -p " Are you sure? [Y/n] " response
case $response in
    [yY][eE][sS]|[yY]) 
        echo -n ' Performing ['$COPY_CMD'] ... '
        `$COPY_CMD`
        echo 'done.'
        echo -n ' Performing ['$ACCESS_CMD'] ... '
        `$ACCESS_CMD`
        echo 'done.'
        echo -n ' Performing ['$ACCESS_CMD2'] ... '
        `$ACCESS_CMD2`
        echo 'done.'
        echo -n ' Performing ['$ACCESS_CMD3'] ... '
        `$ACCESS_CMD3`
        echo 'done.'
        echo -n ' Performing ['$ACCESS_CMD4'] ... '
        `$ACCESS_CMD4`
        echo 'done.'
        echo -n ' Performing ['$ACCESS_CMD5'] ... '
        `$ACCESS_CMD5`
        echo 'done.'


        echo ' Ok, done.'
        ;;
    *)
        echo ' Ok, aborting.'
        ;;
esac


exit
