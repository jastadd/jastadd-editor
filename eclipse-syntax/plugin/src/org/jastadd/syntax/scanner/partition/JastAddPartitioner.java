/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.scanner.partition;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.rules.FastPartitioner;
import org.eclipse.jface.text.rules.IPartitionTokenScanner;

public class JastAddPartitioner extends FastPartitioner {

	public final static String ASPECT_PARTITIONING = "org.jastadd.syntax.aspect.partitioning";
	public final static String FLEX_PARTITIONING = "org.jastadd.syntax.flex.partitioning";
	public final static String GRAMMAR_PARTITIONING = "org.jastadd.syntax.grammar.partitioning";
	public final static String JAVACC_PARTITIONING = "org.jastadd.syntax.javacc.partitioning";
	public final static String PARSER_PARTITIONING = "org.jastadd.syntax.parser.partitioning";
	public final static String TEMPLATE_PARTITIONING = "org.jastadd.syntax.template.partitioning";
	
	public final static String JAVA_DOC = "org.jastadd.syntax.contentType.java.doc";
	public final static String JAVA_MULTI_LINE_COMMENT = "org.jastadd.syntax.contentType.java.multiLineComment";
	public final static String JAVA_SINGLE_LINE_COMMENT = "org.jastadd.syntax.contentType.java.singleLineComment";
	public final static String JAVA_CODE = "org.jastadd.syntax.contentType.java.code";
	public final static String ASPECT_CODE = "org.jastadd.syntax.contentType.aspect.code";
	public final static String TEMPLATE_COMMENT = "org.jastadd.syntax.contentType.template.comment";
	public static final String TEMPLATE_CODE = "org.jastadd.syntax.contentType.template.code";
	public static final String TEMPLATE_BODY = "org.jastadd.syntax.contentType.template.body";
	
	public static final String TEMPLATE_BODY_START = "[[";
	public static final String TEMPLATE_BODY_END = "]]";
	public static final String JAVA_SINGLE_LINE_COMMENT_START = "//";
	public static final String JAVA_SINGLE_LINE_COMMENT_END = "\n";
	public static final String JAVA_MULTI_LINE_COMMENT_START = "/*";
	public static final String JAVA_MULTI_LINE_COMMENT_END = "*/";
	public static final String JAVA_DOC_START = "/**";
	public static final String JAVA_DOC_END = "*/";
	public static final String JAVA_STRING_START = "\"";
	public static final String JAVA_STRING_END = "\"";
	public static final char JAVA_STRING_ESCAPE_CHAR = '\\';
	public static final String JAVA_CHAR_START = "'";
	public static final String JAVA_CHAR_END = "'";
	public static final String PARSER_JAVA_START = "{:";
	public static final String PARSER_JAVA_END = ":}";
	public static final String FLEX_JAVA_START = "%{";
	public static final String FLEX_JAVA_END = "%}";
	public static final String FLEX_JAVA_START2 = "{";
	public static final String FLEX_JAVA_END2 = "}";
	public static final String FLEX_JAVA_INIT_START = "%init{";
	public static final String FLEX_JAVA_INIT_END = "%init}";
	public static final String FLEX_JAVA_INIT_THROW_START = "%initthrow{";
	public static final String FLEX_JAVA_INIT_THROW_END = "%initthrow}";
	public static final String FLEX_THROW_START = "%yylexthrow{";
	public static final String FLEX_THROW_END = "%yylexthrow}";
	public static final String FLEX_EOFVAL_START = "%eofval{";
	public static final String FLEX_EOFVAL_END = "%eofval}";
	public static final String FLEX_EOF_START = "%eof{";
	public static final String FLEX_EOF_END = "%eof}";
	public static final String FLEX_EOFTHROW_START = "%eofthrow{";
	public static final String FLEX_EOFTHROW_END = "%eofthrow}";
	public static final String TEMPLATE_COMMENT_START = "# ";

	public JastAddPartitioner(IPartitionTokenScanner scanner,
			String[] legalContentTypes) {
		super(scanner, legalContentTypes);
	}

	@Override
	public ITypedRegion[] computePartitioning(int offset, int length,
			boolean includeZeroLengthPartitions) {
		ITypedRegion[] result = super.computePartitioning(offset, length, includeZeroLengthPartitions);
		//printPartitions(result);
		return result;
	}

	public void connect(IDocument document, boolean delayInitialise) {
		super.connect(document, delayInitialise);
	}

	public void printPartitions(ITypedRegion[] partitions) {
		StringBuffer buffer = new StringBuffer();
		System.out.println("printPartitions");
		for (int i = 0; i < partitions.length; i++) {
			buffer.append("Partition type: " + partitions[i].getType()
					+ ", offset: " + partitions[i].getOffset()
					+ ", length: " + partitions[i].getLength());
			buffer.append("\n");
			buffer.append("\n---------------------------\n\n\n");
		}
		System.out.print(buffer);
	}
}
