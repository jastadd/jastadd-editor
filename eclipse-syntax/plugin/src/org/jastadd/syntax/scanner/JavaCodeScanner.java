/* Copyright (c) 2005-2013, The JastAdd Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.scanner;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.jastadd.syntax.ColorRegistry;
import org.jastadd.syntax.preferences.PreferenceAccess;
import org.jastadd.syntax.preferences.PreferenceConstants;
import org.jastadd.syntax.scanner.partition.JastAddPartitioner;

public class JavaCodeScanner extends RuleBasedScanner implements
		IPropertyChangeListener {

	// Java keywords
	public static final String[] javaKeywords = { "abstract", "continue",
			"for", "new", "switch", "assert", "default", "if", "package",
			"synchronized", "do", "goto", "private", "this", "break",
			"implements", "protected", "throw", "else", "import", "public",
			"throws", "case", "enum", "instanceof", "transient", "catch",
			"extends", "try", "char", "final", "interface", "static", "void",
			"class", "finally", "strictfp", "volatile", "const", "native",
			"super", "while", "void", "int", "boolean", "float", "long", "int",
			"short", "byte", "double", "boolean", "true", "false", "null" };

	public static final String[] javaKeywordReturn = { "return" };

	// Property listener
	public void propertyChange(PropertyChangeEvent event) {
		String property = event.getProperty();
		// java code
		if (org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_COLOR
				.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_BOLD
						.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_COLOR
						.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_BOLD
						.equals(property)
				||
				// parser colors
				PreferenceConstants.P_PARSER_COLOR.equals(property)
				|| PreferenceConstants.P_PARSER_BOLD.equals(property)
				||
				// flex colors
				PreferenceConstants.P_FLEX_KEYWORD_COLOR.equals(property)
				|| PreferenceConstants.P_FLEX_KEYWORD_BOLD.equals(property)
				|| PreferenceConstants.P_FLEX_PREDEFINED_COLOR.equals(property)
				|| PreferenceConstants.P_FLEX_PREDEFINED_BOLD.equals(property)
				|| PreferenceConstants.P_FLEX_STATE_COLOR.equals(property)
				|| PreferenceConstants.P_FLEX_STATE_BOLD.equals(property)
				|| PreferenceConstants.P_FLEX_REGEXP_COLOR.equals(property)
				|| PreferenceConstants.P_FLEX_REGEXP_BOLD.equals(property)
				||
				// java doc
				org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_DEFAULT_COLOR
						.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_KEYWORD_COLOR
						.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_KEYWORD_BOLD
						.equals(property)
				||
				// java multiline comment
				org.eclipse.jdt.ui.PreferenceConstants.EDITOR_MULTI_LINE_COMMENT_COLOR
						.equals(property)
				||
				// java single-line comment
				org.eclipse.jdt.ui.PreferenceConstants.EDITOR_SINGLE_LINE_COMMENT_COLOR
						.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_COLOR
						.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_BOLD
						.equals(property)) {
			init();
		}
	}

	public JavaCodeScanner(IPreferenceStore store) {
		store.addPropertyChangeListener(this);
		init();
	}

	protected void init() {
		Token defaultToken = new Token(new TextAttribute(ColorRegistry
				.instance().getColor(new RGB(0, 0, 0))));
		List<IRule> rules = new ArrayList<IRule>();
		rules.add(new WhitespaceRule(new WhitespaceDetector()));
		// JavaDoc and Comments, we need to add this to get coloring of this
		// within the JavaCode partition
		addJavaStringCharRules(rules);
		addJavaCommentRules(rules);
		WordRule words = new WordRule(new WordDetector(), defaultToken);
		addJavaWordRules(words);
		rules.add(words);
		addParserToJavaRules(rules);
		addFlexToJavaRules(rules);
		IRule[] result = new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
		setDefaultReturnToken(defaultToken);
	}

	protected static void addFlexToJavaRules(List<IRule> ruleList) {
		Token flexToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getFlexKeywordColor()),
				null, PreferenceAccess.isFlexKeywordBold() ? SWT.BOLD
						: SWT.NORMAL));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_JAVA_START, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_JAVA_END, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_JAVA_INIT_START, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_JAVA_INIT_END, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_JAVA_INIT_THROW_START, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_JAVA_INIT_THROW_END, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_THROW_START, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_THROW_END, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_EOFVAL_START, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_EOFVAL_END, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_EOFTHROW_START, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_EOFTHROW_END, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_EOF_START, flexToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.FLEX_EOF_END, flexToken));
	}

	protected static void addParserToJavaRules(List<IRule> ruleList) {
		Token parserToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getParserColor()), null,
				PreferenceAccess.isParserBold() ? SWT.BOLD : SWT.NORMAL));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.PARSER_JAVA_START, parserToken));
		ruleList.add(new SingleCharacterRule(
				JastAddPartitioner.PARSER_JAVA_END, parserToken));
	}

	protected static void addJavaStringCharRules(List<IRule> ruleList) {
		Token javaStringToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaStringColor())));
		ruleList.add(new SingleLineRule(
				JastAddPartitioner.JAVA_STRING_START,
				JastAddPartitioner.JAVA_STRING_END, javaStringToken,
				JastAddPartitioner.JAVA_STRING_ESCAPE_CHAR));
		ruleList.add(new SingleLineRule(
				JastAddPartitioner.JAVA_CHAR_START,
				JastAddPartitioner.JAVA_CHAR_END, javaStringToken,
				JastAddPartitioner.JAVA_STRING_ESCAPE_CHAR));
	}

	protected static void addJavaCommentRules(List<IRule> ruleList) {
		Token javaSingleLineCommentToken = new Token(new TextAttribute(
				new Color(Display.getDefault(),
						PreferenceAccess.getJavaSingleLineCommentColor())));
		Token javaMultiLineCommentToken = new Token(new TextAttribute(
				new Color(Display.getDefault(),
						PreferenceAccess.getJavaMultiLineCommentColor())));
		Token javaDocToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaDocColor())));
		ruleList.add(new EndOfLineRule(
				JastAddPartitioner.JAVA_SINGLE_LINE_COMMENT_START,
				javaSingleLineCommentToken));
		ruleList.add(new MultiLineRule(JastAddPartitioner.JAVA_DOC_START,
				JastAddPartitioner.JAVA_DOC_END, javaDocToken, (char) 0,
				false));
		ruleList.add(new MultiLineRule(
				JastAddPartitioner.JAVA_MULTI_LINE_COMMENT_START,
				JastAddPartitioner.JAVA_MULTI_LINE_COMMENT_END,
				javaMultiLineCommentToken, (char) 0, false));
	}

	protected static void addJavaWordRules(WordRule words) {
		Token javaKeywordToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaKeywordColor()),
				null, PreferenceAccess.isJavaKeywordBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token javaKeywordReturnToken = new Token(new TextAttribute(new Color(
				Display.getDefault(),
				PreferenceAccess.getJavaKeywordReturnColor()), null,
				PreferenceAccess.isJavaKeywordReturnBold() ? SWT.BOLD
						: SWT.NORMAL));
		for (int i = 0; i < javaKeywords.length; i++) {
			words.addWord(javaKeywords[i], javaKeywordToken);
		}
		for (int i = 0; i < javaKeywordReturn.length; i++) {
			words.addWord(javaKeywordReturn[i], javaKeywordReturnToken);
		}
	}
}
