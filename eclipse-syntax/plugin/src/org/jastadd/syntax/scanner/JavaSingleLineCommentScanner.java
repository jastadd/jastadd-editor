/* Copyright (c) 2005-2013, The JastAdd Team
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.scanner;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.jastadd.syntax.preferences.PreferenceAccess;

public class JavaSingleLineCommentScanner extends RuleBasedScanner {

	// Task tag
	public static final String[] javaTaskTag = { "TASK" };

	// Property listener
	protected IPropertyChangeListener fListener = new PropertyListener();

	private class PropertyListener implements IPropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			String property = event.getProperty();
			if (org.eclipse.jdt.ui.PreferenceConstants.EDITOR_SINGLE_LINE_COMMENT_COLOR
					.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_COLOR
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_BOLD
							.equals(property)) {
				init();
			}
		}
	}

	public JavaSingleLineCommentScanner(IPreferenceStore store) {
		store.addPropertyChangeListener(fListener);
		init();
	}

	protected void init() {
		Token taskToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaTaskTagColor()),
				null, PreferenceAccess.isJavaTaskTagBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token defaultToken = new Token(new TextAttribute(new Color(
				Display.getCurrent(),
				PreferenceAccess.getJavaSingleLineCommentColor())));
		List<IRule> rules = new ArrayList<IRule>();
		rules.add(new WhitespaceRule(new WhitespaceDetector()));
		WordRule words = new WordRule(new WordDetector(), defaultToken);
		for (int i = 0; i < javaTaskTag.length; i++) {
			words.addWord(javaTaskTag[i], taskToken);
		}
		rules.add(words);
		IRule[] result = new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
		setDefaultReturnToken(defaultToken);
	}
}
