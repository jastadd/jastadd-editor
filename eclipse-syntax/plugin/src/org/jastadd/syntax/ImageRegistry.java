/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;

public class ImageRegistry {
	
	private final static IPath ICONS_PATH = new Path("icons/");
	
	public static final String IMG_ASPECT_FILE = "IMG_ASPECT_FILE";
	public static final String IMG_GRAMMAR_FILE = "IMG_GRAMMAR_FILE";
	public static final String IMG_PARSER_FILE = "IMG_PARSER_FILE";
	public static final String IMG_FLEX_FILE = "IMG_FLEX_FILE";
	public static final String IMG_TEMPLATE_FILE = "IMG_TEMPLATE_FILE";
	
	static {
		registerImage(IMG_ASPECT_FILE, "aspect_16x16.png");
		registerImage(IMG_GRAMMAR_FILE, "grammar_16x16.png");
		registerImage(IMG_PARSER_FILE, "parser_16x16.png");
		registerImage(IMG_FLEX_FILE, "flex_16x16.png");
		registerImage(IMG_TEMPLATE_FILE, "template_16x16.png");
	}
	
	private static void registerImage(String key, String name) {
		IPath path = ICONS_PATH.append(name);
		registerImage(key, Activator.getDefault().getBundle(), path);
	}
	
	public static Image getImage(String key) {
		return Activator.getDefault().getImageRegistry().get(key);
	}
	
	protected static void registerImage(String key, Bundle bundle, IPath path) {
		ImageDescriptor imageDescriptor = createImageDescriptor(bundle, path);
		Activator.getDefault().getImageRegistry().put(key, imageDescriptor);
	}
	
	protected static ImageDescriptor createImageDescriptor(Bundle bundle, IPath path) {
		URL url= FileLocator.find(bundle, path, null);
		if (url != null) {
			return ImageDescriptor.createFromURL(url);
		}
		return ImageDescriptor.getMissingImageDescriptor();
	}
}
