/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.flex;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.jastadd.syntax.ColorRegistry;
import org.jastadd.syntax.preferences.PreferenceAccess;
import org.jastadd.syntax.preferences.PreferenceConstants;
import org.jastadd.syntax.scanner.SingleCharacterRule;
import org.jastadd.syntax.scanner.WhitespaceDetector;
import org.jastadd.syntax.scanner.partition.JastAddPartitioner;

public class FlexScanner extends RuleBasedScanner {

	private static String[] javaKeywords = { "package", "import" };

	private static String[] flexConstants = { "\\n", "\\t", "\\f", "\\r" };

	private static String[] flexTokens = { "+", "*", "?", "!", "~", "^", "|",
			"[", "]", "$", "-" };

	private static final String PREDEFINED_START = "[:";
	private static final String PREDEFINED_END = ":]";
	private static final String STATE_START = "<";
	private static final String STATE_END = ">";
	private static final String EOF_STATE_START = "<<";
	private static final String EOF_STATE_END = ">>";

	private static String[] flexKeywords = { "%%", "%function", "%type",
			"%yylexthrow", "%line", "%column", "%char", "%state", "%xstate",
			"%cup", "%cupdebug", "%cupsym", "%byacc", "%apiprivate", "%debug",
			"%standalone", "%switch", "%table", "%pack", "%initthrow",
			"%ctorarg", "%scanerror", "%buffer", "%include", "%int",
			"%integer", "%intwrap", "%unicode", "%16bit", "%7bit", "%8bit",
			"%full", "%caseless", "ignorecase", "%eofclose", "%public",
			"%final", "%class", "%extends", "%implements", "%abstract" };

	// Property listener
	protected IPropertyChangeListener fListener = new PropertyListener();

	private class PropertyListener implements IPropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			String property = event.getProperty();
			// java code
			if (org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_COLOR
					.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_BOLD
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_COLOR
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_BOLD
							.equals(property)
					||
					// flex code
					PreferenceConstants.P_FLEX_KEYWORD_COLOR.equals(property)
					|| PreferenceConstants.P_FLEX_KEYWORD_BOLD.equals(property)
					|| PreferenceConstants.P_FLEX_PREDEFINED_COLOR
							.equals(property)
					|| PreferenceConstants.P_FLEX_PREDEFINED_BOLD
							.equals(property)
					|| PreferenceConstants.P_FLEX_STATE_COLOR.equals(property)
					|| PreferenceConstants.P_FLEX_STATE_BOLD.equals(property)
					|| PreferenceConstants.P_FLEX_REGEXP_COLOR.equals(property)
					|| PreferenceConstants.P_FLEX_REGEXP_BOLD.equals(property)) {
				init();
			}
		}
	}

	public FlexScanner(IPreferenceStore store) {
		store.addPropertyChangeListener(fListener);
		init();
	}

	protected void init() {
		Token javaKeywordToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaKeywordColor()),
				null, PreferenceAccess.isJavaKeywordBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token javaStringToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaStringColor())));
		Token flexKeywordToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getFlexKeywordColor()),
				null, PreferenceAccess.isFlexKeywordBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token flexPredefinedToken = new Token(
				new TextAttribute(new Color(Display.getDefault(),
						PreferenceAccess.getFlexPredefinedColor()), null,
						PreferenceAccess.isFlexPredefinedBold() ? SWT.BOLD
								: SWT.NORMAL));
		Token flexStateToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getFlexStateColor()),
				null, PreferenceAccess.isFlexStateBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token flexRegExpToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getFlexRegExpColor()),
				null, PreferenceAccess.isFlexRegExpBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token defaultToken = new Token(new TextAttribute(ColorRegistry
				.instance().getColor(new RGB(0, 0, 0))));
		// Add rules
		List<IRule> rules = new ArrayList<IRule>();
		rules.add(new SingleLineRule(JastAddPartitioner.JAVA_STRING_START,
				JastAddPartitioner.JAVA_STRING_END, javaStringToken,
				JastAddPartitioner.JAVA_STRING_ESCAPE_CHAR));
		rules.add(new SingleLineRule(JastAddPartitioner.JAVA_CHAR_START,
				JastAddPartitioner.JAVA_CHAR_END, javaStringToken,
				JastAddPartitioner.JAVA_STRING_ESCAPE_CHAR));
		rules.add(new WhitespaceRule(new WhitespaceDetector()));
		WordRule words = new WordRule(new FlexWordDetector(), defaultToken);
		// Flex keywords
		for (int i = 0; i < flexKeywords.length; i++) {
			words.addWord(flexKeywords[i], flexKeywordToken);
		}
		rules.add(new SingleCharacterRule("\\\"", Token.UNDEFINED));
		rules.add(new SingleCharacterRule("\\\'", Token.UNDEFINED));
		// Java strings and characters
		for (int i = 0; i < javaKeywords.length; i++) {
			words.addWord(javaKeywords[i], javaKeywordToken);
		}
		rules.add(words);
		for (int i = 0; i < flexConstants.length; i++) {
			rules.add(new SingleCharacterRule(flexConstants[i], javaStringToken));
		}
		// Predefined flex tokens
		rules.add(new SingleLineRule(PREDEFINED_START, PREDEFINED_END,
				flexPredefinedToken));
		// Flex tokens
		for (int i = 0; i < flexTokens.length; i++) {
			rules.add(new SingleCharacterRule(flexTokens[i], flexRegExpToken));
		}
		// Flex state
		rules.add(new SingleLineRule(EOF_STATE_START, EOF_STATE_END,
				flexStateToken));
		rules.add(new SingleLineRule(STATE_START, STATE_END, flexStateToken));
		IRule[] result = new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
		setDefaultReturnToken(defaultToken);
	}

	public static class FlexWordDetector implements IWordDetector {
		@Override
		public boolean isWordStart(char c) {
			return Character.isJavaIdentifierStart(c) || c == '%';
		}
		@Override
		public boolean isWordPart(char c) {
			return Character.isJavaIdentifierPart(c) || c == '%';
		}
	}
}
