/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>, 
 * Jesper Öqvist <jesper.oqvist@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.template;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.jastadd.syntax.autoedit.JavaCommentAutoEditStrategy;
import org.jastadd.syntax.autoedit.JavaDocAutoEditStrategy;
import org.jastadd.syntax.scanner.JavaCodeScanner;
import org.jastadd.syntax.scanner.JavaDocScanner;
import org.jastadd.syntax.scanner.JavaMultiLineCommentScanner;
import org.jastadd.syntax.scanner.JavaSingleLineCommentScanner;
import org.jastadd.syntax.scanner.partition.JastAddPartitioner;

public class TemplateSourceViewerConfiguration extends SourceViewerConfiguration {

	protected IPreferenceStore fPreferenceStore;

	public TemplateSourceViewerConfiguration(IPreferenceStore pStore) {
		super();
		fPreferenceStore = pStore;
	}

	/*
	 * SYNTAX HIGHLIGHTING
	 * 
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.text.source.SourceViewerConfiguration#
	 * getPresentationReconciler(org.eclipse.jface.text.source.ISourceViewer)
	 */
	@Override
	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		
		PresentationReconciler reconciler = new PresentationReconciler();
		
		reconciler.setDocumentPartitioning(JastAddPartitioner.TEMPLATE_PARTITIONING);
		
		// Template scanner
		ITokenScanner scanner = new TemplateScanner(fPreferenceStore);
		DefaultDamagerRepairer dr = new DefaultDamagerRepairer(scanner);
		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);
		
		// Template body scanner
		scanner = new TemplateBodyScanner(fPreferenceStore);
		dr = new DefaultDamagerRepairer(scanner);
		reconciler.setDamager(dr, JastAddPartitioner.TEMPLATE_BODY);
		reconciler.setRepairer(dr, JastAddPartitioner.TEMPLATE_BODY);
		
		// Java code scanner
		scanner = new JavaCodeScanner(fPreferenceStore);
		dr = new DefaultDamagerRepairer(scanner);
		reconciler.setDamager(dr, JastAddPartitioner.JAVA_CODE);
		reconciler.setRepairer(dr, JastAddPartitioner.JAVA_CODE);
		
		// Documentation scanner
		scanner = new JavaDocScanner(fPreferenceStore);
		dr = new DefaultDamagerRepairer(scanner);
		reconciler.setDamager(dr, JastAddPartitioner.JAVA_DOC);
		reconciler.setRepairer(dr, JastAddPartitioner.JAVA_DOC);
		
		// Multi line comment scanner
		scanner = new JavaMultiLineCommentScanner(fPreferenceStore);
		dr = new DefaultDamagerRepairer(scanner);
		reconciler.setDamager(dr, JastAddPartitioner.JAVA_MULTI_LINE_COMMENT);
		reconciler.setRepairer(dr, JastAddPartitioner.JAVA_MULTI_LINE_COMMENT);
		
		// Single line comment scanner
		scanner = new JavaSingleLineCommentScanner(fPreferenceStore);
		dr = new DefaultDamagerRepairer(scanner);
		reconciler.setDamager(dr, JastAddPartitioner.JAVA_SINGLE_LINE_COMMENT);
		reconciler.setRepairer(dr, JastAddPartitioner.JAVA_SINGLE_LINE_COMMENT);
		
		return reconciler;
	}
	
	@Override
	public String getConfiguredDocumentPartitioning(ISourceViewer sourceViewer) {
		return JastAddPartitioner.TEMPLATE_PARTITIONING;
	}

	@Override
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer) {
		return new String[] { IDocument.DEFAULT_CONTENT_TYPE,
				JastAddPartitioner.TEMPLATE_BODY,
				JastAddPartitioner.JAVA_DOC,
				JastAddPartitioner.JAVA_CODE,
				JastAddPartitioner.JAVA_MULTI_LINE_COMMENT,
				JastAddPartitioner.JAVA_SINGLE_LINE_COMMENT };
	}

	/*
	 * AUTO EDIT (INDENTATION)
	 * 
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.text.source.SourceViewerConfiguration#getAutoEditStrategies
	 * (org.eclipse.jface.text.source.ISourceViewer, java.lang.String)
	 */
	@Override
	public IAutoEditStrategy[] getAutoEditStrategies(
			ISourceViewer sourceViewer, String contentType) {
		if (contentType.equals(JastAddPartitioner.JAVA_DOC)) {
			return new IAutoEditStrategy[] { new JavaDocAutoEditStrategy(
					fPreferenceStore) };
		} else if (contentType
				.equals(JastAddPartitioner.JAVA_MULTI_LINE_COMMENT)) {
			return new IAutoEditStrategy[] { new JavaCommentAutoEditStrategy(
					fPreferenceStore) };
		}
		// return new IAutoEditStrategy[] { AspectAutoEditStrategy.instance() };
		// Or use the super behavior, same as not overriding this method
		return super.getAutoEditStrategies(sourceViewer, contentType);
	}
}
