/* Copyright (c) 2013, 
 * Jesper Öqvist <jesper.oqvist@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.template;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

public class TemplateDoubleBracketsRule implements IRule {
	final private Token token = new Token(new TextAttribute(new Color(
			Display.getDefault(), new RGB(0, 0, 0)), null, SWT.BOLD));
	
	static final private IToken undefined = new IToken() {
		@Override
		public boolean isWhitespace() {
			return false;
		}
		@Override
		public boolean isUndefined() {
			return true;
		}
		@Override
		public boolean isOther() {
			return false;
		}
		@Override
		public boolean isEOF() {
			return false;
		}
		@Override
		public Object getData() {
			return null;
		}
	};

	@Override
	public IToken evaluate(ICharacterScanner scanner) {
		int ch = scanner.read();
		if (ch == '[') {
			if (scanner.read() == '[') {
				return token;
			}
			scanner.unread();
		} else if (ch == ']') {
			if (scanner.read() == ']') {
				return token;
			}
			scanner.unread();
		}
		scanner.unread();
		return undefined;
	}
}
