/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>, 
 * Jesper Öqvist <jesper.oqvist@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.template;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.jastadd.syntax.ColorRegistry;
import org.jastadd.syntax.preferences.PreferenceAccess;
import org.jastadd.syntax.preferences.PreferenceConstants;
import org.jastadd.syntax.scanner.JavaCodeScanner;
import org.jastadd.syntax.scanner.WhitespaceDetector;
import org.jastadd.syntax.scanner.WordDetector;
import org.jastadd.syntax.scanner.partition.JastAddPartitioner;

public class TemplateScanner extends RuleBasedScanner implements IPropertyChangeListener {

	public TemplateScanner(IPreferenceStore store) {
		store.addPropertyChangeListener(this);
		init();
	}
	
	// Property change
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		String property = event.getProperty();
		// java code
		if (org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_COLOR
				.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_BOLD
				.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_COLOR
				.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_BOLD
				.equals(property)
				||
				// java doc
				org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_DEFAULT_COLOR
				.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_KEYWORD_COLOR
				.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_KEYWORD_BOLD
				.equals(property)
				||
				// java multi-line comment
				org.eclipse.jdt.ui.PreferenceConstants.EDITOR_MULTI_LINE_COMMENT_COLOR
				.equals(property)
				||
				// java single-line comment
				org.eclipse.jdt.ui.PreferenceConstants.EDITOR_SINGLE_LINE_COMMENT_COLOR
				.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_COLOR
				.equals(property)
				|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_BOLD
				.equals(property)
				// template comments
				|| PreferenceConstants.P_TEMPLATE_COMMENT_COLOR.equals(property)) {
			init();
		}
	}

	protected void init() {
		Token templateCommentToken = new Token(new TextAttribute(new Color(
				Display.getDefault(),
				PreferenceAccess.getTemplateCommentColor()), null, SWT.BOLD));
		Token defaultToken = new Token(new TextAttribute(ColorRegistry
				.instance().getColor(new RGB(0, 0, 0)), null, SWT.BOLD));
		List<IRule> rules = new ArrayList<IRule>();
		// template comment
		rules.add(new EndOfLineRule(
				JastAddPartitioner.TEMPLATE_COMMENT_START,
				templateCommentToken));
		WordRule words = new WordRule(new WordDetector(), defaultToken);
		// Java keywords
		addJavaWordRules(words);
		rules.add(words);
		addJavaStringCharRules(rules);
		rules.add(new WhitespaceRule(new WhitespaceDetector()));
		IRule[] result = new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
		setDefaultReturnToken(defaultToken);
	}
	

	protected static void addJavaWordRules(WordRule words) {
		Token javaKeywordToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaKeywordColor()),
				null, PreferenceAccess.isJavaKeywordBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token javaKeywordReturnToken = new Token(new TextAttribute(new Color(
				Display.getDefault(),
				PreferenceAccess.getJavaKeywordReturnColor()), null,
				PreferenceAccess.isJavaKeywordReturnBold() ? SWT.BOLD
						: SWT.NORMAL));
		for (int i = 0; i < JavaCodeScanner.javaKeywords.length; i++) {
			words.addWord(JavaCodeScanner.javaKeywords[i], javaKeywordToken);
		}
		for (int i = 0; i < JavaCodeScanner.javaKeywordReturn.length; i++) {
			words.addWord(JavaCodeScanner.javaKeywordReturn[i], javaKeywordReturnToken);
		}
	}
	
	protected static void addJavaStringCharRules(List<IRule> ruleList) {
		Token javaStringToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaStringColor())));
		ruleList.add(new SingleLineRule(
				JastAddPartitioner.JAVA_STRING_START,
				JastAddPartitioner.JAVA_STRING_END, javaStringToken,
				JastAddPartitioner.JAVA_STRING_ESCAPE_CHAR));
		ruleList.add(new SingleLineRule(
				JastAddPartitioner.JAVA_CHAR_START,
				JastAddPartitioner.JAVA_CHAR_END, javaStringToken,
				JastAddPartitioner.JAVA_STRING_ESCAPE_CHAR));
	}
}
