/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.grammar;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.jastadd.syntax.ColorRegistry;
import org.jastadd.syntax.preferences.PreferenceAccess;
import org.jastadd.syntax.preferences.PreferenceConstants;
import org.jastadd.syntax.scanner.SingleCharacterRule;
import org.jastadd.syntax.scanner.WhitespaceDetector;
import org.jastadd.syntax.scanner.WordDetector;

public class GrammarScanner extends RuleBasedScanner {

	// TODO: Use class attributes in rule definitions in init() instead of
	// strings

	public static final String[] astKeywords = { "abstract", "package",
			"import", "boolean", "int", "float", "double", "short", "long",
			"byte" };

	public static final String[] astStructKeywords = { "::=", // Production
			"<", ">", // Lexical tokens or inter-AST reference
			"/", "/" // NTAs
	};

	public static final String[] astOccurence = { "[", "]", // Optional
			"*" // Multiple
	};

	public static final String[] astInheritance = { ":", // Inheritance
	};

	// Property listener
	protected IPropertyChangeListener fListener = new PropertyListener();

	private class PropertyListener implements IPropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			String property = event.getProperty();
			// grammar code
			if (PreferenceConstants.P_GRAMMAR_PRODUCTION_COLOR.equals(property)
					|| PreferenceConstants.P_GRAMMAR_INHERITANCE_COLOR
							.equals(property)
					|| PreferenceConstants.P_GRAMMAR_OPTIONAL_COLOR
							.equals(property)
					|| PreferenceConstants.P_GRAMMAR_MULTIPLE_COLOR
							.equals(property)
					|| PreferenceConstants.P_GRAMMAR_NTA_COLOR.equals(property)
					|| PreferenceConstants.P_GRAMMAR_LEXEM_COLOR
							.equals(property)
					|| PreferenceConstants.P_GRAMMAR_PRODUCTION_BOLD
							.equals(property)
					|| PreferenceConstants.P_GRAMMAR_INHERITANCE_BOLD
							.equals(property)
					|| PreferenceConstants.P_GRAMMAR_OPTIONAL_BOLD
							.equals(property)
					|| PreferenceConstants.P_GRAMMAR_MULTIPLE_BOLD
							.equals(property)
					|| PreferenceConstants.P_GRAMMAR_NTA_BOLD.equals(property)
					|| PreferenceConstants.P_GRAMMAR_LEXEM_BOLD
							.equals(property)
					||
					// java code
					org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_COLOR
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_BOLD
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_COLOR
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_BOLD
							.equals(property)) {
				init();
			}
		}
	}

	public GrammarScanner(IPreferenceStore store) {
		store.addPropertyChangeListener(fListener);
		init();
	}

	protected void init() {
		Token astKeywordToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaKeywordColor()),
				null, PreferenceAccess.isJavaKeywordBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token astLexemToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getGrammarLexemColor()),
				null, PreferenceAccess.isGrammarLexemBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token astNTAToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getGrammarNTAColor()),
				null, PreferenceAccess.isGrammarNTABold() ? SWT.BOLD
						: SWT.NORMAL));
		Token astProdToken = new Token(new TextAttribute(new Color(
				Display.getDefault(),
				PreferenceAccess.getGrammarProductionColor()), null,
				PreferenceAccess.isGrammarProductionBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token astOptionalToken = new Token(new TextAttribute(new Color(
				Display.getDefault(),
				PreferenceAccess.getGrammarOptionalColor()), null,
				PreferenceAccess.isGrammarOptionalBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token astMultipleToken = new Token(new TextAttribute(new Color(
				Display.getDefault(),
				PreferenceAccess.getGrammarMultipleColor()), null,
				PreferenceAccess.isGrammarMultipleBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token astInheritanceToken = new Token(new TextAttribute(new Color(
				Display.getDefault(),
				PreferenceAccess.getGrammarInheritanceColor()), null,
				PreferenceAccess.isGrammarInheritanceBold() ? SWT.BOLD
						: SWT.NORMAL));
		Token defaultToken = new Token(new TextAttribute(ColorRegistry
				.instance().getColor(new RGB(0, 0, 0))));
		List<IRule> rules = new ArrayList<IRule>();
		rules.add(new WhitespaceRule(new WhitespaceDetector()));
		WordRule words = new WordRule(new WordDetector(), defaultToken);
		// AST tokens
		for (int i = 0; i < astKeywords.length; i++) {
			words.addWord(astKeywords[i], astKeywordToken);
		}
		rules.add(words);
		// Optional
		rules.add(new SingleCharacterRule("[", astOptionalToken));
		rules.add(new SingleCharacterRule("]", astOptionalToken));
		// Multiple
		rules.add(new SingleCharacterRule("*", astMultipleToken));
		// Production
		rules.add(new SingleCharacterRule("::=", astProdToken));
		// Inheritance
		rules.add(new SingleCharacterRule(":", astInheritanceToken));
		// NTA
		rules.add(new SingleCharacterRule("/", astNTAToken));
		// Lexem
		rules.add(new SingleCharacterRule("<", astLexemToken));
		rules.add(new SingleCharacterRule(">", astLexemToken));
		// Transform and set
		IRule[] result = new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
		setDefaultReturnToken(defaultToken);
	}
}
