/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.grammar;

import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.RuleBasedPartitionScanner;
import org.eclipse.jface.text.rules.Token;
import org.jastadd.syntax.scanner.partition.JastAddPartitioner;

public class GrammarPartitionScanner extends RuleBasedPartitionScanner {
	
	public GrammarPartitionScanner() {
		super();	
		IToken doc = new Token(JastAddPartitioner.JAVA_DOC);
		IToken singleLineComment = new Token(JastAddPartitioner.JAVA_SINGLE_LINE_COMMENT);
		IToken multiLineComment = new Token(JastAddPartitioner.JAVA_MULTI_LINE_COMMENT);
		setPredicateRules(new IPredicateRule[] {
				new MultiLineRule(JastAddPartitioner.JAVA_MULTI_LINE_COMMENT_START,
						JastAddPartitioner.JAVA_MULTI_LINE_COMMENT_END, 
						multiLineComment, (char) 0, false),
				new MultiLineRule(JastAddPartitioner.JAVA_DOC_START,
						JastAddPartitioner.JAVA_DOC_END, doc, (char) 0, false),
				new EndOfLineRule(JastAddPartitioner.JAVA_SINGLE_LINE_COMMENT_START,
						singleLineComment)		
		});
	}
}
