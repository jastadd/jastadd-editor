/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.javacc;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.jastadd.syntax.ColorRegistry;
import org.jastadd.syntax.preferences.PreferenceAccess;
import org.jastadd.syntax.preferences.PreferenceConstants;
import org.jastadd.syntax.scanner.JavaCodeScanner;
import org.jastadd.syntax.scanner.WhitespaceDetector;
import org.jastadd.syntax.scanner.WordDetector;

/**
 * Base scanner for JavaCC files (.jj, .jjt).
 */
public class JavaCCScanner extends JavaCodeScanner {

	// JavaCC keywords
	protected static String[] javaccKeywords = { "options", "TOKEN", "SKIP",
			"MORE", "SPECIAL_TOKEN", "PARSER_BEGIN", "PARSER_END", "JAVACODE" };

	// JavaCC options
	protected static String[] javaccOptions = { "LOOKAHEAD",
			"CHOICE_AMBIGUITY_CHECK", "OTHER_AMBIGUITY_CHECK", "STATIC",
			"SUPPORT_CLASS_VISIBILITY_PUBLIC", "DEBUG_PARSER",
			"DEBUG_LOOKAHEAD", "DEBUG_TOKEN_MANAGER", "ERROR_REPORTING",
			"JAVA_UNICODE_ESCAPE", "UNICODE_INPUT", "IGNORE_CASE",
			"USER_TOKEN_MANAGER", "USER_CHAR_STREAM", "BUILD_PARSER",
			"BUILD_TOKEN_MANAGER", "TOKEN_EXTENDS", "TOKEN_FACTORY",
			"TOKEN_MANAGER_USES_PARSER", "SANITY_CHECK", "FORCE_LA_CHECK",
			"COMMON_TOKEN_ACTION", "CACHE_TOKENS", "OUTPUT_DIRECTORY",
			"VISITOR", "MULTI", "NODE_DEFAULT_VOID", "NODE_PACKAGE",
			"NODE_SCOPE_HOOK" };

	// Property change
	@Override
	public void propertyChange(PropertyChangeEvent event) {
		super.propertyChange(event);
		String property = event.getProperty();
		if (PreferenceConstants.P_JAVACC_KEYWORD_COLOR.equals(property)
				|| PreferenceConstants.P_JAVACC_KEYWORD_BOLD.equals(property)
				|| PreferenceConstants.P_JAVACC_OPTION_COLOR.equals(property)
				|| PreferenceConstants.P_JAVACC_OPTION_BOLD.equals(property)) {
			init();
		}
	}

	public JavaCCScanner(IPreferenceStore store) {
		super(store);
	}

	protected void init() {
		Token javaccKeywordToken = new Token(new TextAttribute(
				new Color(Display.getDefault(),
						PreferenceAccess.getJavaCCKeywordColor()), null,
				PreferenceAccess.isJavaCCKeywordBold() ? SWT.BOLD : SWT.NORMAL));
		Token javaccOptionToken = new Token(new TextAttribute(new Color(
				Display.getDefault(), PreferenceAccess.getJavaCCOptionColor()),
				null, PreferenceAccess.isJavaCCOptionBold() ? SWT.BOLD
						: SWT.NORMAL));

		Token defaultToken = new Token(new TextAttribute(ColorRegistry
				.instance().getColor(new RGB(0, 0, 0))));
		List<IRule> rules = new ArrayList<IRule>();
		WordRule words = new WordRule(new WordDetector(), defaultToken);
		// JavaCC keywords
		for (int i = 0; i < javaccKeywords.length; i++) {
			words.addWord(javaccKeywords[i], javaccKeywordToken);
		}
		// JavaCC options
		for (int i = 0; i < javaccOptions.length; i++) {
			words.addWord(javaccOptions[i], javaccOptionToken);
		}
		addJavaCommentRules(rules);
		// Java keywords
		addJavaWordRules(words);
		rules.add(words);
		addJavaStringCharRules(rules);
		rules.add(new WhitespaceRule(new WhitespaceDetector()));
		IRule[] result = new IRule[rules.size()];
		rules.toArray(result);
		setRules(result);
		setDefaultReturnToken(defaultToken);
	}
}
