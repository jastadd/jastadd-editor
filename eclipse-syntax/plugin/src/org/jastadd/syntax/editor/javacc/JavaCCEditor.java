/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.javacc;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;
import org.eclipse.ui.texteditor.ChainedPreferenceStore;
import org.jastadd.syntax.Activator;
import org.jastadd.syntax.editor.aspect.AspectDocumentProvider;
import org.jastadd.syntax.preferences.PreferenceConstants;

public class JavaCCEditor extends AbstractDecoratedTextEditor {

	/** The ID of this editor as defined in plugin.xml */
	public static final String EDITOR_ID = "org.jastadd.syntax.editor.javacc";

	/** The ID of the editor context menu */
	public static final String EDITOR_CONTEXT = EDITOR_ID + ".context";

	protected IPreferenceStore fPreferenceStore;

	public JavaCCEditor() {
		super();
		fPreferenceStore = createCombinedPreferenceStore();
		setSourceViewerConfiguration(new JavaCCSourceViewerConfiguration(
				fPreferenceStore));
		setDocumentProvider(new JavaCCDocumentProvider());
	}

	/*
	 * PREFERENCE STORE
	 */
	protected IPreferenceStore createCombinedPreferenceStore() {
		List<IPreferenceStore> stores = new ArrayList<IPreferenceStore>(3);
		stores.add(Activator.getDefault().getPreferenceStore());
		stores.add(JavaPlugin.getDefault().getPreferenceStore());
		stores.add(EditorsUI.getPreferenceStore());
		stores.add(PlatformUI.getPreferenceStore());
		return new ChainedPreferenceStore(
				stores.toArray(new IPreferenceStore[stores.size()]));
	}

	/*
	 * PREFERENCE CHANGES
	 */
	@Override
	protected void handlePreferenceStoreChanged(PropertyChangeEvent event) {
		try {
			boolean change = false;
			String property = event.getProperty();
			// Multi line comment
			if (org.eclipse.jdt.ui.PreferenceConstants.EDITOR_MULTI_LINE_COMMENT_COLOR
					.equals(property)) {
				change = true;
			}
			// Single line comment
			if (org.eclipse.jdt.ui.PreferenceConstants.EDITOR_SINGLE_LINE_COMMENT_COLOR
					.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_COLOR
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_BOLD
							.equals(property)) {
				change = true;
			}
			// Java doc
			if (org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_DEFAULT_COLOR
					.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_KEYWORD_COLOR
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_KEYWORD_BOLD
							.equals(property)) {
				change = true;
			}
			// Java code
			if (org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_COLOR
					.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_BOLD
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_COLOR
							.equals(property)
					|| org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_BOLD
							.equals(property)) {
				change = true;
			}
			// JavaCC code
			if (PreferenceConstants.P_JAVACC_KEYWORD_COLOR.equals(property)
					|| PreferenceConstants.P_JAVACC_KEYWORD_BOLD
							.equals(property)
					|| PreferenceConstants.P_JAVACC_OPTION_COLOR
							.equals(property)
					|| PreferenceConstants.P_JAVACC_OPTION_BOLD
							.equals(property)) {
				change = true;
			}
			if (change) {
				getSourceViewer().invalidateTextPresentation();
			}
		} finally {
			super.handlePreferenceStoreChanged(event);
		}
	}
}
