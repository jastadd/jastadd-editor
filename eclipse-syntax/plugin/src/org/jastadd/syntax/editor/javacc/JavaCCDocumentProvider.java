/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.editor.javacc;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentExtension3;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
import org.jastadd.syntax.scanner.partition.JastAddPartitioner;

public class JavaCCDocumentProvider extends TextFileDocumentProvider {
	
	@Override
	public IDocument getDocument(Object element) {
		IDocument document = super.getDocument(element);		
		if (document instanceof IDocumentExtension3 && 
				((IDocumentExtension3) document).getDocumentPartitioner(
						JastAddPartitioner.JAVACC_PARTITIONING) == null) {
			IDocumentPartitioner partitioner = new JastAddPartitioner(
					new JavaCCPartitionScanner(), new String[] {
						IDocument.DEFAULT_CONTENT_TYPE,
						JastAddPartitioner.JAVA_MULTI_LINE_COMMENT,
						JastAddPartitioner.JAVA_DOC,
						JastAddPartitioner.JAVA_SINGLE_LINE_COMMENT });
			partitioner.connect(document);
			((IDocumentExtension3)document).setDocumentPartitioner(JastAddPartitioner.JAVACC_PARTITIONING, partitioner);
		}
		return document;
	}
}
