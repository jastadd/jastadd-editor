/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.RGB;
import org.jastadd.syntax.Activator;
import org.jastadd.syntax.ColorRegistry;

public class PreferenceInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		// match brackets by default
		store.setDefault(PreferenceConstants.P_MATCH_BRACKETS, true);
		// use gray for matching bracket drawing
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_MATCHING_BRACKETS_COLOR,
				ColorRegistry.COLOR_GREY);
		// JastAdd style
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_ASPECT_KEYWORD_COLOR,
				ColorRegistry.COLOR_FORREST_GREEN);
		store.setDefault(PreferenceConstants.P_ASPECT_KEYWORD_BOLD, true);
		// JavaCC style
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_JAVACC_KEYWORD_COLOR,
				ColorRegistry.COLOR_RED);
		store.setDefault(PreferenceConstants.P_JAVACC_KEYWORD_BOLD, true);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_JAVACC_OPTION_COLOR,
				ColorRegistry.COLOR_FORREST_GREEN);
		store.setDefault(PreferenceConstants.P_JAVACC_OPTION_BOLD, true);
		// grammar style
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_GRAMMAR_PRODUCTION_COLOR,
				ColorRegistry.COLOR_PURPLE);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_GRAMMAR_INHERITANCE_COLOR,
				ColorRegistry.COLOR_PURPLE);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_GRAMMAR_OPTIONAL_COLOR,
				ColorRegistry.COLOR_PURPLE);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_GRAMMAR_MULTIPLE_COLOR,
				ColorRegistry.COLOR_PURPLE);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_GRAMMAR_NTA_COLOR,
				ColorRegistry.COLOR_PURPLE);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_GRAMMAR_LEXEM_COLOR,
				ColorRegistry.COLOR_PURPLE);
		store.setDefault(PreferenceConstants.P_GRAMMAR_PRODUCTION_BOLD, true);
		store.setDefault(PreferenceConstants.P_GRAMMAR_INHERITANCE_BOLD, true);
		store.setDefault(PreferenceConstants.P_GRAMMAR_OPTIONAL_BOLD, true);
		store.setDefault(PreferenceConstants.P_GRAMMAR_MULTIPLE_BOLD, true);
		store.setDefault(PreferenceConstants.P_GRAMMAR_NTA_BOLD, true);
		store.setDefault(PreferenceConstants.P_GRAMMAR_LEXEM_BOLD, true);
		// Parser style
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_PARSER_COLOR,
				ColorRegistry.COLOR_MIDDLE_BLUE);
		store.setDefault(PreferenceConstants.P_PARSER_BOLD, true);
		// Flex style
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_FLEX_KEYWORD_COLOR,
				ColorRegistry.COLOR_LIGHT_ORANGE);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_FLEX_PREDEFINED_COLOR,
				ColorRegistry.COLOR_LIGHT_ORANGE);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_FLEX_STATE_COLOR,
				ColorRegistry.COLOR_LIGHT_ORANGE);
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_FLEX_REGEXP_COLOR,
				ColorRegistry.COLOR_LIGHT_ORANGE);
		store.setDefault(PreferenceConstants.P_FLEX_KEYWORD_BOLD, true);
		store.setDefault(PreferenceConstants.P_FLEX_PREDEFINED_BOLD, false);
		store.setDefault(PreferenceConstants.P_FLEX_STATE_BOLD, true);
		store.setDefault(PreferenceConstants.P_FLEX_REGEXP_BOLD, true);
		// Template style
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_TEMPLATE_COMMENT_COLOR,
				new RGB(233, 0, 23));
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_TEMPLATE_ATTRIBUTE_COLOR,
				new RGB(20, 146, 20));
		PreferenceConverter.setDefault(store,
				PreferenceConstants.P_TEMPLATE_VARIABLE_COLOR,
				new RGB(20, 146, 20));
	}
}
