/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.jastadd.syntax.Activator;

public class GrammarEditorPreferencePage extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	public GrammarEditorPreferencePage() {
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Settings for the JastAdd Grammar Editor");
	}

	@Override
	protected void createFieldEditors() {
		// Grammar production
		addField(new ColorFieldEditor(
				PreferenceConstants.P_GRAMMAR_PRODUCTION_COLOR,
				"&Production color", getFieldEditorParent()));
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_GRAMMAR_PRODUCTION_BOLD,
				"Production &bold", getFieldEditorParent()));
		// Grammar inheritance
		addField(new ColorFieldEditor(
				PreferenceConstants.P_GRAMMAR_INHERITANCE_COLOR,
				"&Inheritance color", getFieldEditorParent()));
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_GRAMMAR_INHERITANCE_BOLD,
				"Inheritance &bold", getFieldEditorParent()));
		// Grammar optional
		addField(new ColorFieldEditor(
				PreferenceConstants.P_GRAMMAR_OPTIONAL_COLOR,
				"&Optional color", getFieldEditorParent()));
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_GRAMMAR_OPTIONAL_BOLD, "Optional &bold",
				getFieldEditorParent()));
		// Grammar multiple
		addField(new ColorFieldEditor(
				PreferenceConstants.P_GRAMMAR_MULTIPLE_COLOR,
				"&Multiple color", getFieldEditorParent()));
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_GRAMMAR_MULTIPLE_BOLD, "Multiple &bold",
				getFieldEditorParent()));
		// Grammar NTA
		addField(new ColorFieldEditor(PreferenceConstants.P_GRAMMAR_NTA_COLOR,
				"&NTA color", getFieldEditorParent()));
		addField(new BooleanFieldEditor(PreferenceConstants.P_GRAMMAR_NTA_BOLD,
				"NTA &bold", getFieldEditorParent()));
		// Grammar lexem
		addField(new ColorFieldEditor(
				PreferenceConstants.P_GRAMMAR_LEXEM_COLOR, "&Lexem color",
				getFieldEditorParent()));
		addField(new BooleanFieldEditor(
				PreferenceConstants.P_GRAMMAR_LEXEM_BOLD, "Lexem &bold",
				getFieldEditorParent()));
	}

	@Override
	public void init(IWorkbench workbench) {
	}
}
