/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.preferences;

public class PreferenceConstants {

	public static final String P_MATCH_BRACKETS = "matchBrackets";
	public static final String P_MATCHING_BRACKETS_COLOR = "matchingBracketsColor";

	public static final String P_ASPECT_KEYWORD_COLOR = "aspectKeywordColor";
	public static final String P_ASPECT_KEYWORD_BOLD = "aspectKeywordBold";

	public static final String P_GRAMMAR_PRODUCTION_COLOR = "grammarProductionColor";
	public static final String P_GRAMMAR_PRODUCTION_BOLD = "grammarProductionBold";
	public static final String P_GRAMMAR_INHERITANCE_COLOR = "grammarInheritanceColor";
	public static final String P_GRAMMAR_INHERITANCE_BOLD = "grammarInheritanceBold";
	public static final String P_GRAMMAR_OPTIONAL_COLOR = "grammarOptionalColor";
	public static final String P_GRAMMAR_OPTIONAL_BOLD = "grammarOptionalBold";
	public static final String P_GRAMMAR_MULTIPLE_COLOR = "grammarMultipleColor";
	public static final String P_GRAMMAR_MULTIPLE_BOLD = "grammarMultipleBold";
	public static final String P_GRAMMAR_NTA_COLOR = "grammarNTAColor";
	public static final String P_GRAMMAR_NTA_BOLD = "grammarNTABold";
	public static final String P_GRAMMAR_LEXEM_COLOR = "grammarLexemColor";
	public static final String P_GRAMMAR_LEXEM_BOLD = "grammarLexemBold";

	public static final String P_PARSER_COLOR = "parserColor";
	public static final String P_PARSER_BOLD = "parserBold";

	public static final String P_FLEX_KEYWORD_COLOR = "flexKeywordColor";
	public static final String P_FLEX_KEYWORD_BOLD = "flexKeywordBold";
	public static final String P_FLEX_PREDEFINED_COLOR = "flexPredefinedColor";
	public static final String P_FLEX_PREDEFINED_BOLD = "flexPredefinedBold";
	public static final String P_FLEX_STATE_COLOR = "flexStateColor";
	public static final String P_FLEX_STATE_BOLD = "flexStateBold";
	public static final String P_FLEX_REGEXP_COLOR = "flexRegexpColor";
	public static final String P_FLEX_REGEXP_BOLD = "flexRegexpBold";

	public static final String P_TEMPLATE_COMMENT_COLOR = "templateCommentColor";
	public static final String P_TEMPLATE_ATTRIBUTE_COLOR = "templateAttributeColor";
	public static final String P_TEMPLATE_VARIABLE_COLOR = "templateVariableColor";
	
	public static final String P_JAVACC_KEYWORD_COLOR = "javaccKeywordColor";
	public static final String P_JAVACC_KEYWORD_BOLD = "javaccKeywordBold";
	public static final String P_JAVACC_OPTION_COLOR = "javaccOptionColor";
	public static final String P_JAVACC_OPTION_BOLD = "javaccOptionBold";
}
