/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.preferences;

import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;
import org.jastadd.syntax.Activator;

public class PreferenceAccess {

	public static boolean getMatchBrackets() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_MATCH_BRACKETS);
	}

	public static RGB getMatchingBracketsColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_MATCHING_BRACKETS_COLOR);
	}

	// TinyTemplate

	public static RGB getTemplateCommentColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_TEMPLATE_COMMENT_COLOR);
	}
	
	public static RGB getTemplateAttributeColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_TEMPLATE_ATTRIBUTE_COLOR);
	}
	
	public static RGB getTemplateVariableColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_TEMPLATE_VARIABLE_COLOR);
	}

	// JavaCC 

	public static RGB getJavaCCKeywordColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_JAVACC_KEYWORD_COLOR);
	}
	
	public static boolean isJavaCCKeywordBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_JAVACC_KEYWORD_BOLD);
	}

	public static RGB getJavaCCOptionColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_JAVACC_OPTION_COLOR);
	}
	
	public static boolean isJavaCCOptionBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_JAVACC_OPTION_BOLD);
	}

	// Aspect keywords

	public static RGB getJastaddKeywordColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_ASPECT_KEYWORD_COLOR);
	}

	public static boolean isJastaddKeywordBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_ASPECT_KEYWORD_BOLD);
	}

	// Grammar keywords

	public static RGB getGrammarProductionColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_GRAMMAR_PRODUCTION_COLOR);
	}

	public static RGB getGrammarInheritanceColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_GRAMMAR_INHERITANCE_COLOR);
	}

	public static RGB getGrammarOptionalColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_GRAMMAR_OPTIONAL_COLOR);
	}

	public static RGB getGrammarMultipleColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_GRAMMAR_MULTIPLE_COLOR);
	}

	public static RGB getGrammarNTAColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(), PreferenceConstants.P_GRAMMAR_NTA_COLOR);
	}

	public static RGB getGrammarLexemColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_GRAMMAR_LEXEM_COLOR);
	}

	public static boolean isGrammarProductionBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_GRAMMAR_PRODUCTION_BOLD);
	}

	public static boolean isGrammarInheritanceBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_GRAMMAR_INHERITANCE_BOLD);
	}

	public static boolean isGrammarOptionalBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_GRAMMAR_OPTIONAL_BOLD);
	}

	public static boolean isGrammarMultipleBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_GRAMMAR_MULTIPLE_BOLD);
	}

	public static boolean isGrammarNTABold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_GRAMMAR_NTA_BOLD);
	}

	public static boolean isGrammarLexemBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_GRAMMAR_LEXEM_BOLD);
	}

	// Parser color

	public static RGB getParserColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(), PreferenceConstants.P_PARSER_COLOR);
	}

	public static boolean isParserBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_PARSER_BOLD);
	}

	// Flex color

	public static RGB getFlexKeywordColor() {
		return PreferenceConverter
				.getColor(Activator.getDefault().getPreferenceStore(),
						PreferenceConstants.P_FLEX_KEYWORD_COLOR);
	}

	public static RGB getFlexPredefinedColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(),
				PreferenceConstants.P_FLEX_PREDEFINED_COLOR);
	}

	public static RGB getFlexStateColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(), PreferenceConstants.P_FLEX_STATE_COLOR);
	}

	public static RGB getFlexRegExpColor() {
		return PreferenceConverter.getColor(Activator.getDefault()
				.getPreferenceStore(), PreferenceConstants.P_FLEX_REGEXP_COLOR);
	}

	public static boolean isFlexKeywordBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_FLEX_KEYWORD_BOLD);
	}

	public static boolean isFlexPredefinedBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_FLEX_PREDEFINED_BOLD);
	}

	public static boolean isFlexStateBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_FLEX_STATE_BOLD);
	}

	public static boolean isFlexRegExpBold() {
		return Activator.getDefault().getPreferenceStore()
				.getBoolean(PreferenceConstants.P_FLEX_REGEXP_BOLD);
	}

	// Preference settings from JDT

	public static RGB getJavaMultiLineCommentColor() {
		return PreferenceConverter
				.getColor(
						JavaPlugin.getDefault().getPreferenceStore(),
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_MULTI_LINE_COMMENT_COLOR);
	}

	public static RGB getJavaSingleLineCommentColor() {
		return PreferenceConverter
				.getColor(
						JavaPlugin.getDefault().getPreferenceStore(),
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_SINGLE_LINE_COMMENT_COLOR);
	}

	public static RGB getJavaTaskTagColor() {
		return PreferenceConverter.getColor(JavaPlugin.getDefault()
				.getPreferenceStore(),
				org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_COLOR);
	}

	public static boolean isJavaTaskTagBold() {
		return JavaPlugin
				.getDefault()
				.getPreferenceStore()
				.getBoolean(
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_TASK_TAG_BOLD);
	}

	public static RGB getJavaStringColor() {
		return PreferenceConverter.getColor(JavaPlugin.getDefault()
				.getPreferenceStore(),
				org.eclipse.jdt.ui.PreferenceConstants.EDITOR_STRING_COLOR);
	}

	public static RGB getJavaDocColor() {
		return PreferenceConverter
				.getColor(
						JavaPlugin.getDefault().getPreferenceStore(),
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_DEFAULT_COLOR);
	}

	public static RGB getJavaDocKeywordColor() {
		return PreferenceConverter
				.getColor(
						JavaPlugin.getDefault().getPreferenceStore(),
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_KEYWORD_COLOR);
	}

	public static boolean isJavaDocKeywordBold() {
		return JavaPlugin
				.getDefault()
				.getPreferenceStore()
				.getBoolean(
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVADOC_KEYWORD_BOLD);
	}

	public static RGB getJavaKeywordColor() {
		return PreferenceConverter
				.getColor(
						JavaPlugin.getDefault().getPreferenceStore(),
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_COLOR);
	}

	public static boolean isJavaKeywordBold() {
		return JavaPlugin
				.getDefault()
				.getPreferenceStore()
				.getBoolean(
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_BOLD);
	}

	public static RGB getJavaKeywordReturnColor() {
		return PreferenceConverter
				.getColor(
						JavaPlugin.getDefault().getPreferenceStore(),
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_COLOR);
	}

	public static boolean isJavaKeywordReturnBold() {
		return JavaPlugin
				.getDefault()
				.getPreferenceStore()
				.getBoolean(
						org.eclipse.jdt.ui.PreferenceConstants.EDITOR_JAVA_KEYWORD_RETURN_BOLD);
	}

	// Editor settings
	public static int getEditorTabWidth() {
		return EditorsUI
				.getPreferenceStore()
				.getInt(AbstractDecoratedTextEditorPreferenceConstants.EDITOR_TAB_WIDTH);
	}

	public static boolean getEditorSpacesForTabs() {
		return EditorsUI
				.getPreferenceStore()
				.getBoolean(
						AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS);
	}
}
