/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.autoedit;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultIndentLineAutoEditStrategy;
import org.eclipse.jface.text.DocumentCommand;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;

public class JavaCommentAutoEditStrategy extends
		DefaultIndentLineAutoEditStrategy {

	// Property listener
	protected IPropertyChangeListener fListener = new PropertyListener();

	private class PropertyListener implements IPropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			String property = event.getProperty();
			if (AbstractDecoratedTextEditorPreferenceConstants.EDITOR_TAB_WIDTH
					.equals(property)
					|| AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS
							.equals(property)) {
				init();
			}
		}
	}

	public JavaCommentAutoEditStrategy(IPreferenceStore store) {
		store.addPropertyChangeListener(fListener);
		init();
	}

	protected void init() {
	}

	private void autoIndentAfterNewLine(IDocument d, DocumentCommand c) {
		if (c.offset == -1 || d.getLength() == 0)
			return;
		try {
			// find start of line
			int p = (c.offset == d.getLength() ? c.offset - 1 : c.offset);
			IRegion info = d.getLineInformationOfOffset(p);
			int start = info.getOffset();
			// find white spaces
			int end = findEndOfWhiteSpace(d, start, c.offset);
			// Append indent
			StringBuffer buf = new StringBuffer(c.text);
			if (end > start) {
				// append to input
				buf.append(d.get(start, end - start));
			}
			// Insert line start
			if (d.getChar(end) == '/') {
				// Move one step to the right the first time
				buf.append(" ");
			}
			// Add a star at the beginning of the line
			buf.append("*");
			String indent = buf.toString();
			// Insert end of comment if line contains start of comment
			String preEdit = d.get(c.offset - 2, c.offset);
			String postEdit = d.get(c.offset + indent.length() - 1, c.offset
					+ indent.length());
			if (preEdit.startsWith("/*") && !postEdit.startsWith("*")) {
				buf.append(" " + indent + "/\n");
				// Perform command
				c.text = buf.toString();
				d.replace(c.offset, c.length, c.text);
				// Move caret
				c.text = "";
				c.length = 0;
				int newCaretPosition = c.offset + indent.length() + 1;
				c.offset = newCaretPosition;
				d.replace(c.offset, c.length, c.text);
			} else {
				buf.append(" ");
				c.text = buf.toString();
			}
		} catch (BadLocationException excp) {
			// stop work
		}
	}

	private void insertEndToken(IDocument d, DocumentCommand c) {
		if (c.offset == -1 || d.getLength() == 0)
			return;
		try {
			String s = d.get(c.offset - 2, c.offset);
			if (s.startsWith("*")) {
				c.offset--;
			}
		} catch (BadLocationException excp) {
			// stop work
		}
	}

	public void customizeDocumentCommand(IDocument d, DocumentCommand c) {
		if (c.length == 0
				&& c.text != null
				&& TextUtilities.endsWith(d.getLegalLineDelimiters(), c.text) != -1) {
			autoIndentAfterNewLine(d, c);
		} else if (c.text != null && c.text.equals("/")) {
			insertEndToken(d, c);
		}
	}
}
