/* Copyright (c) 2013, 
 * Emma Söderberg <emma.soderberg@cs.lth.se>.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.syntax.autoedit;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultIndentLineAutoEditStrategy;
import org.eclipse.jface.text.DocumentCommand;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.TextUtilities;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;
import org.jastadd.syntax.preferences.PreferenceAccess;

public class AspectAutoEditStrategy extends DefaultIndentLineAutoEditStrategy{

	// Property listener
	protected IPropertyChangeListener fListener = new PropertyListener();
	private class PropertyListener implements IPropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent event) {
			String property = event.getProperty();
			if (AbstractDecoratedTextEditorPreferenceConstants.EDITOR_TAB_WIDTH.equals(property) ||
					AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS.equals(property)) {
				init();
			}
		}
	}
	
	public AspectAutoEditStrategy(IPreferenceStore store) { 
		store.addPropertyChangeListener(fListener);
		init();
	}

	protected void init() {
		setTabWith(PreferenceAccess.getEditorTabWidth());
		setSpacesForTabs(PreferenceAccess.getEditorSpacesForTabs());
	}
	
	protected int fTabWidth;
	protected boolean fSpacesForTabs;
	
	public void setTabWith(int value) {
		fTabWidth = value;
	}
	public void setSpacesForTabs(boolean value) {
		fSpacesForTabs = value;
	}
	
	protected String getIndentation() {
		if (!fSpacesForTabs) {
			return "\t";
		}
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < fTabWidth; i++) {
			buf.append(" ");
		}
		return buf.toString();
	}
	
	public void customizeDocumentCommand(IDocument d, DocumentCommand c) {
		if (c.length == 0 && c.text != null && TextUtilities.endsWith(d.getLegalLineDelimiters(), c.text) != -1) {
			autoIndentAfterNewLine(d, c);
		}
		else if (c.text != null && c.text.equals(")")) {
			insertEndParenToken(d, c);
		} 
		else if (c.text != null && c.text.equals("(")) {
			insertStartParenToken(d, c);
		}
		else if (c.text != null && c.text.equals("]")) {
			insertEndBracketToken(d, c);
		} 
		else if (c.text != null && c.text.equals("[")) {
			insertStartBracketToken(d, c);
		}
		else if (c.text != null && c.text.equals("\"")) {
			insertStringToken(d,c);
		}
		else if (c.text != null && c.text.equals("'")) {
			insertCharToken(d,c);
		}
	}
	
	private void autoIndentAfterNewLine(IDocument d, DocumentCommand c) { 
		if (c.offset == -1 || d.getLength() == 0) 
			return;
		try {
			// Current line
			// find start of line
			int p = (c.offset == d.getLength() ? c.offset  - 1 : c.offset);
			IRegion info = d.getLineInformationOfOffset(p);
			int start = info.getOffset();
			// find white spaces
			int end = findEndOfWhiteSpace(d, start, c.offset);
			
			// Append indent to input
			StringBuffer buf = new StringBuffer(c.text);
			if (end > start) {
				buf.append(d.get(start, end - start));
			} 
			String preIndent = buf.toString();
			
			// Increase indentation level ? 
			String preEdit = d.get(start, c.offset-start);
			if (preEdit.trim().endsWith("{")) {
				buf.append(getIndentation());
				
				// Do this here in case any of the below fails and throws an exception
				c.text = buf.toString();
				
				/*
				// Insert end brace?
				
				// Larger indent? -- Don't insert end brace
				IRegion info2 = d.getLineInformation(p+1);
				int start2 = info2.getOffset();
				int end2 = findEndOfWhiteSpace(d, start2, info2.getOffset() + info2.getLength());
				
				// Existing end brace?
				String postEdit = d.get(start2, end2 - start2);				
				if (end2 - start2 <= preIndent.length() && 
						!postEdit.trim().startsWith("}")) {

					buf.append(preIndent + "}");

					// Perform command
					c.text = buf.toString();
					d.replace(c.offset, c.length, c.text);

					// Move caret
					c.text = "";
					c.length = 0;
					int newCaretPosition = c.offset + preIndent.length() + 1;
					c.offset = newCaretPosition;
					d.replace(c.offset, c.length, c.text);
				}
				// There is an end brace -- don't insert another		
				*/	 
			} 
			// Increase indent after = in an equation
			else if (preEdit.trim().endsWith("=")) {
				buf.append(getIndentation());
				c.text = buf.toString();
			}
			// Just a new line
			else {
				c.text = buf.toString();	
			}
			/*
			String indent = buf.toString();
			
			// Insert end of comment if line contains start of comment
			String preEdit = d.get(c.offset-1, c.offset);
			String postEdit = d.get(c.offset + indent.length() - 1, c.offset +  indent.length());
            if (preEdit.startsWith("{") && !postEdit.startsWith("}")) {
            	buf.append(indent + "}");
         
            	// Perform command
            	c.text = buf.toString();
            	d.replace(c.offset, c.length, c.text);
            	
            	// Move caret
            	c.text = "";
            	c.length = 0;
            	int newCaretPosition = c.offset + indent.length();
            	c.offset = newCaretPosition;
            	d.replace(c.offset, c.length, c.text);
            	
			} else {
				c.text = buf.toString();
			}
			*/
			
		} catch (BadLocationException excp) {
			// stop work
		}
	}
	
	private void insertEndParenToken(IDocument d, DocumentCommand c) {
		if (c.offset == -1 || d.getLength() == 0)
			return;
		try {			
			// Don't insert another end parenthesis if there is one already
            String s = d.get(c.offset, c.offset+1);
            if (s.startsWith(")")) {
				c.offset++;
				c.text = "";
			}	
		} catch (BadLocationException excp) {
			// stop work
		}		
	}
	
	private void insertStartParenToken(IDocument d, DocumentCommand c) {
		if (c.offset == -1 || d.getLength() == 0)
			return;
		try {	
			// Don't insert another end parenthesis if there is one already
            String s = d.get(c.offset, c.offset+1);
            if (!s.startsWith(")") && !s.startsWith("(")) {
				
				// Perform command
    			c.text += ")";
            	d.replace(c.offset, c.length, c.text);
            	
            	// Move caret
            	c.text = "";
            	c.length = 0;
            	int newCaretPosition = c.offset + 1;
            	c.offset = newCaretPosition;
            	d.replace(c.offset, c.length, c.text);
			} 
		} catch (BadLocationException excp) {
			// stop work
		}		
	}
	
	private void insertEndBracketToken(IDocument d, DocumentCommand c) {
		if (c.offset == -1 || d.getLength() == 0)
			return;
		try {
			// Don't insert another end parenthesis if there is one already
            String s = d.get(c.offset, c.offset+1);
            if (s.startsWith("]")) {
				c.offset++;
				c.text = "";
			}
		} catch (BadLocationException excp) {
			// stop work
		}		
	}
	
	private void insertStartBracketToken(IDocument d, DocumentCommand c) {
		if (c.offset == -1 || d.getLength() == 0)
			return;
		try {
			// Don't insert another end parenthesis if there is one already
            String s = d.get(c.offset, c.offset+1);
            if (!s.startsWith("]") && !s.startsWith("[")) {
				// Perform command
    			c.text += "]";
            	d.replace(c.offset, c.length, c.text);
            	// Move caret
            	c.text = "";
            	c.length = 0;
            	int newCaretPosition = c.offset + 1;
            	c.offset = newCaretPosition;
            	d.replace(c.offset, c.length, c.text);
			} 
		} catch (BadLocationException excp) {
			// stop work
		}		
	}
	
	private void insertStringToken(IDocument d, DocumentCommand c) {
		if (c.offset == -1 || d.getLength() == 0)
			return;
		try {
			// Don't insert another string token if there is one already
            String s = d.get(c.offset, 1);
            if (!s.startsWith("\"")) {
				// Perform command
    			c.text += "\"";
            	d.replace(c.offset, c.length, c.text);
            	// Move caret
            	c.text = "";
            	c.length = 0;
            	int newCaretPosition = c.offset + 1;
            	c.offset = newCaretPosition;
            	d.replace(c.offset, c.length, c.text);
			} 
            // If there is a " already jump over it
            else {
				// Move caret
            	c.text = "";
            	c.length = 0;
            	int newCaretPosition = c.offset + 1;
            	c.offset = newCaretPosition;
            	d.replace(c.offset, c.length, c.text);
			}
		} catch (BadLocationException excp) {
			// stop work
		}		
	}

	private void insertCharToken(IDocument d, DocumentCommand c) {
		if (c.offset == -1 || d.getLength() == 0)
			return;
		try {
			// Don't insert another char token if there is one already
            String s = d.get(c.offset, 1);
            if (!s.startsWith("'")) {
				// Perform command
    			c.text += "'";
            	d.replace(c.offset, c.length, c.text);
            	// Move caret
            	c.text = "";
            	c.length = 0;
            	int newCaretPosition = c.offset + 1;
            	c.offset = newCaretPosition;
            	d.replace(c.offset, c.length, c.text);
			}
            // If there is a ' already jump over it
            else {
				// Move caret
            	c.text = "";
            	c.length = 0;
            	int newCaretPosition = c.offset + 1;
            	c.offset = newCaretPosition;
            	d.replace(c.offset, c.length, c.text);
			}
		} catch (BadLocationException excp) {
			// stop work
		}		
	}	
}
