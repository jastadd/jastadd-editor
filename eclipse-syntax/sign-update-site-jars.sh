#!/bin/bash

KEYSTORE=$HOME'/.keystore-jastadd'
ALIAS='jastadd'

echo ' This will sign the following files: '
echo ''
for file in `ls update-site/plugins/*.jar update-site/features/*.jar`
do
  echo '  file: ['$file']'
done
echo ''
echo '  keystore: ['$KEYSTORE']'
echo '  alias:    ['$ALIAS']'
echo ''
echo '  NB! You will be prompted for the password for the above keystore/alias.'
echo ''
read -r -p '  Are you sure? [Y/n] ' response
case $response in
    [yY][eE][sS]|[yY]) 
  read -p "  Enter password: " -r -s PWD
#  echo '['$PWD']'
  for file in `ls update-site/plugins/*.jar update-site/features/*.jar`
  do
    echo '  Signing ['$file']'
    WARN=`jarsigner -keystore $KEYSTORE -storepass $PWD -keypass $PWD $file $ALIAS`
    echo '  '$WARN
    #jarsigner -verify $file
   done
   echo '  Ok, done.'
   ;;
   *)
   echo '  Ok, aborting.'
   ;;
esac

exit

