 JastAdd Syntax Editors
 ========
 
Project intended for development of an Eclipse plugin providing editors with syntax highlighting for files used in JastAdd projects (.jrag,.jadd,.parser,.flex,.jj,.jjt, and .tt).

 Release procedure
 -------

 * Updating plugin and feature version. 
 
 * Update feature version in the update site project.

 * Build update site.

 * Sign jar files with:
  
    > ./sign-update-site-jars.sh

 * Publish upudate site with:

    > ./publish-update-site.sh
