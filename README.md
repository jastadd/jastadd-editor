JastAddEditor
=============
 
This project is intended for editor solutions for files used by the JastAdd
system.

Installing
----------

You can install the Eclipse plugin by adding an update site with the URL
`http://jastadd.org/update-site/eclipse-syntax` in Eclipse. This is done by
selecting the "Install New Software..." menu item in the "Help" menu.

Testing the Plugin
------------------

You will need Eclipse RCP edition in order to work with the JastAddEditor
plugin.

Start Eclipse RCP and import the eclipse-syntax/plugin project.

You can test the plugin by right-clicking the plugin project and then selecting
Run As->Ecplise Application.
